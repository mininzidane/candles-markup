$(function () {

	// product carousel clicker
	$('.js-image-list').on('click', 'a', function () {
		var $this = $(this),
			imageUrl = $this.attr('href'),
			imageId = $this.closest('.js-image-list').data('target');

		$(imageId).fadeOut('fast', function() {
			$(this).attr('src', imageUrl).fadeIn('slow');
		});
		return false;
	});
});